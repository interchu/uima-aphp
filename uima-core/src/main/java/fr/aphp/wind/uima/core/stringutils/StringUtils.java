/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.core.stringutils;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class StringUtils {

	public static String normaliseCase(String s) {
		char firstLetter = s.charAt(0);
		String nextLetters = s.substring(1, s.length()).toLowerCase();
		String allLetters = firstLetter + nextLetters;
		return allLetters;
	}

	public static String stripAccents(String s) {
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}

	public static String toSingular() {
		return "";
	}
	
	public static boolean doContainVowel(String str) {
		Pattern pat = Pattern.compile("(?i)(?u)[aeyiuo]");
		return pat.matcher(str).find();
	}
	
	public static boolean doContainConsonant(String str) {
		Pattern pat = Pattern.compile("(?i)(?u)[^aeyiuo]");
		return pat.matcher(str).find();
	}
	public static boolean doContainDigit(String str){
		return Pattern.compile("\\d").matcher(str).find();

	}
	
	public static boolean isDigitPunct(String str){
		return Pattern.compile("[\\d\\p{Punct}]+").matcher(str).matches();

	}
	
	public static boolean lookLastName(String str){
		return 	doContainVowel(str) 
				&&  doContainConsonant(str)
				&& !doContainDigit(str)
				&& Pattern.compile(ListDeid.RECUP_LAST_NAME_REGEX).matcher(str).matches();
				
	}
	
	public static boolean lookMajLastName(String str){
		return 	doContainVowel(str) 
				&&  doContainConsonant(str)
				&& !doContainDigit(str)
				&& Pattern.compile(ListDeid.RECUP_LAST_NAME_REGEX).matcher(str).matches();
				
	}
	
	public static boolean lookFirstName(String str){
		return !doContainDigit(str) 
				&& Pattern.compile(ListDeid.RECUP_FIRST_NAME_REGEX).matcher(str).matches();

	}
	public static boolean doContainMaj(String str){
		return Pattern.compile("\\p{Lu}").matcher(str).find();
	}
	
	public static boolean doNotContainMaj(String str){
		return !doContainMaj(str);
	}
	
	public static boolean isFirstLowerCase(String str){
		return 
				!Pattern.compile("^[\\p{Lu}]").matcher(str).find()
				 && !isDigitPunct(str.substring(0,1));
	}
	
	public static boolean isFirstUpperCase(String str){
		return Pattern.compile("^[\\p{Lu}]").matcher(str).find();
	}

}

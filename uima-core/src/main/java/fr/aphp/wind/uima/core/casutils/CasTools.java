/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.core.casutils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;

import com.google.common.collect.Lists;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;

import org.apache.log4j.Logger;

import fr.aphp.wind.uima.core.stringutils.StringDistance;

public class CasTools {
	private static Logger logger = Logger.getLogger(CasTools.class);

	// sens > 0 when right of the anchor
	// sens < 0 when left of the anchor
	public static boolean isNumberWordBetween(JCas aJCas, AnnotationFS anchor, Collection<Type> typesToFind,
			Type tokenType, int sens, int nomberToken, Collection<String> tokenToExclude, boolean acceptEndSentence) {
		// <91000>, <St-Ouen> -> 1
		// <91000> dans la ville de <St-Ouen> -> 2
		for (Type aType : typesToFind) {
			boolean foundEnd = false;
			AnnotationFS a = null;
			try {
				a = CasUtil.selectSingleRelative(aJCas.getCas(), aType, anchor, sens);
			} catch (Exception e) {
			}

			if (a != null) {
				int sizeWords = 0;
				List<AnnotationFS> b = CasUtil.selectBetween(aJCas.getCas(), tokenType, a, anchor);
				if (sens < 0) { // on part en sens inverse
					b = Lists.reverse(b);
				}
				for (AnnotationFS anno : b) {// pour chaque token entre
					if (Pattern.compile("[\\.\\!\\?:;]+").matcher(anno.getCoveredText()).matches()
							&& acceptEndSentence) {
						foundEnd = true;
					}
					for (String exclud : tokenToExclude) {
						Pattern ePattern = Pattern.compile(exclud);
						if (!ePattern.matcher(anno.getCoveredText()).matches()) {
							sizeWords++;
						}

					}
				}
				if (tokenToExclude.size() == 0) {
					sizeWords = b.size();

				}
				// System.out.println(String.format("Search: %s & Found token:
				// %s at %d", anchor.getCoveredText(),
				// a.getCoveredText(), b.size()));

				if (sizeWords <= nomberToken && !foundEnd)
					return true;
			}
		}
		return false;
	}
	
	public static boolean isRegexInSentence(JCas aJCas, AnnotationFS anno, Collection<String> patterns){
		List<Sentence> sents = JCasUtil.selectCovering(Sentence.class, anno);
		if(sents.size()>0){
			Sentence sent = sents.get(0);
			
			for(String pattern:patterns){
				
			if(Pattern.compile(pattern).matcher(sent.getCoveredText()).find()){
				
			return true;
			}
			}
		}else{
			
		}
		return false;
	}

	// numberChar > 0 when right of the anchor
	// numberChar < 0 when left of the anchor
	public static boolean isRegexAroundAnnot(JCas aJCas, AnnotationFS anchor, Collection<String> regexesToFind,
			int numberChar) {
		for (String regex : regexesToFind) {
			int begin;
			int end;
			if (numberChar > 0) {// annot ... <regex>
				begin = anchor.getEnd();
				end = Math.min(aJCas.getDocumentText().length(), anchor.getEnd() + numberChar);
			} else {// <regex> ... annot
				begin = Math.max(0, anchor.getBegin() + numberChar);
				end = anchor.getBegin();
			}
			String text = aJCas.getDocumentText().substring(begin, end);
			Pattern pat = Pattern.compile(regex);
			// if(anchor.getCoveredText().equals("Paris"))
			// System.out.println(String.format("Trouvé:%b '%s' in:
			// %s",pat.matcher(text).find(), regex, text));
			if (pat.matcher(text).find())
				return true;

		}
		return false;
	}

	// numberChar > 0 when right of the anchor
	// numberChar < 0 when left of the anchor
	public static boolean isRegexAroundAnnotExact(JCas aJCas, AnnotationFS anchor, Collection<String> regexesToFind,
			int numberChar) {
		for (String regex : regexesToFind) {
			int begin;
			int end;
			String rgAppend = "";
			if (numberChar > 0) {// annot ... <regex>
				// regex += "[\\s\\S]*";
				begin = anchor.getEnd();
				end = Math.min(aJCas.getDocumentText().length(), anchor.getEnd() + numberChar);
			} else {// <regex> ... annot
				// regex = "[\\s\\S]*" + regex;
				begin = Math.max(0, anchor.getBegin() + numberChar);
				end = anchor.getBegin();
			}
			String text = aJCas.getDocumentText().substring(begin, end);
			Pattern pat = Pattern.compile(regex);
			// if(anchor.getCoveredText().equals("Paris"))
			// System.out.println(String.format("Trouvé:%b '%s' in:
			// %s",pat.matcher(text).find(), regex, text));
			if (pat.matcher(text).matches()) {

				return true;
			}

		}
		return false;
	}

	// numberWords > 0 when right of the anchor
	// numberWords < 0 when left of the anchor
	public static boolean isTokenAroundAnnot(JCas aJCas, Type tokenAnnot, AnnotationFS anchor,
			Collection<String> tokenToFind, int numberWords) {
		List<AnnotationFS> listToken;
		if (numberWords > 0) {// annot ... <token>
			listToken = CasUtil.selectFollowing(aJCas.getCas(), tokenAnnot, anchor, numberWords);

		} else {// <token> ... annot
			listToken = CasUtil.selectPreceding(aJCas.getCas(), tokenAnnot, anchor, numberWords);

		}

		for (String token : tokenToFind) {
			for (AnnotationFS tokens : listToken) {
				if (token.toLowerCase().equals(tokens.getCoveredText().toLowerCase()))
					return true;
			}
		}
		return false;
	}

	public static void propagate(JCas aJCas, String annoToPropagate) {
		Type annoToPropagateTypes = CasUtil.getType(aJCas.getCas(), annoToPropagate);
		Type annotationToken = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");

		for (AnnotationFS annoToPropagateType : CasUtil.select(aJCas.getCas(), annoToPropagateTypes)) {
			for (String splitedAnnoToken : annoToPropagateType.getCoveredText().trim().split("[\\s-]+")) {
				for (AnnotationFS token : CasUtil.select(aJCas.getCas(), annotationToken)) {
					for (String splitedToken : token.getCoveredText().trim().split("[\\s-]+")) {

						if (splitedToken.length() > 2 // do not propagate to
														// small words
								// && distKernel.isSimilar(splitedAnnoToken ,
								// splitedToken, true)
								&& splitedAnnoToken.toLowerCase().equals(splitedToken.toLowerCase())) {// add
																										// all
																										// the
																										// token
							if (!hasType(aJCas, annoToPropagate, token)) {
								AnnotationFS b = aJCas.getCas().createAnnotation(annoToPropagateTypes, token.getBegin(),
										token.getEnd());
								((TOP) b).addToIndexes();
								logger.info(
										String.format("Propagate %s : >%s< ", b.getType(), CasTools.getTextContent(b)));

							}
						}
					}
				}
			}
		}
	}

	public static boolean hasType(JCas aJCas, String type, AnnotationFS token) {
		Type annoToPropagateTypes = CasUtil.getType(aJCas.getCas(), type);
		List<AnnotationFS> types = CasUtil.selectCovering(annoToPropagateTypes, token);
		for (AnnotationFS typed : types) {
			if (typed.getType().equals(annoToPropagateTypes)) {
				// System.out.println("found"+ token.getCoveredText() +
				// typed.getType().toString());
				return true;
			}
		}
		return false;
	}

	public static String getPos(JCas aJCas, AnnotationFS token) {
		Type annotationToken = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");

		List<AnnotationFS> types = CasUtil.selectCovering(annotationToken, token);
		if (types.size() > 0)
			return ((de.unihd.dbs.uima.types.heideltime.Token) types.get(0)).getPos();

		List<AnnotationFS> type2 = CasUtil.selectCovered(annotationToken, token);
		if (type2.size() > 0)
			return ((de.unihd.dbs.uima.types.heideltime.Token) type2.get(0)).getPos();

		return "NOTHING";
	}

	public static boolean isPrecededByNewline(JCas aJCas, AnnotationFS token) {
		Type annotationToken = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");

		List<AnnotationFS> types = CasUtil.selectPreceding(aJCas.getCas(), annotationToken, token, 1);
		if (types.size() > 0) {
			String morceau = aJCas.getDocumentText().substring(types.get(0).getEnd(), token.getBegin());
			if (Pattern.compile("\\n").matcher(morceau).find()) {
				System.out.println("premier mot: " + token.getCoveredText());
				return true;
			}
		}
		return false;

	}

	public static String getTextContent(FeatureStructure fs) {

		int begin = ((Annotation) fs).getBegin();
		int end = ((Annotation) fs).getEnd();

		return fs.getCAS().getDocumentText().substring(begin, end);

	}

	public static void explodeMolecules(JCas aJCas, ArrayList<String> annos) {

		for (String anno : annos) {
			Type annoToPropagateTypes = CasUtil.getType(aJCas.getCas(), anno);
			Type annotationToken = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");

			for (AnnotationFS annoToPropagateType : CasUtil.select(aJCas.getCas(), annoToPropagateTypes)) {
				List<AnnotationFS> tokens = CasUtil.selectCovered(annotationToken, annoToPropagateType);
				if(tokens != null && tokens.size()==1){
					continue;
				}
				for (AnnotationFS tok : tokens) {
					AnnotationFS b = aJCas.getCas().createAnnotation( annoToPropagateType.getType(), tok.getBegin(),
							tok.getEnd());
					((TOP) b).addToIndexes();
					logger.warn(String.format("Exploded %s : >%s<  of type >%s<", annoToPropagateType.getCoveredText(),
							CasTools.getTextContent(b), annoToPropagateType.getType().getName()));
				}
				TOP test = (TOP) annoToPropagateType;
				//test.removeFromIndexes();
			}
		}
	}
}

/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.tools;

import org.junit.Test;

public class DeidTest {

  @Test
  public void TestSimple() {
    // 0123456789
    String monCR = "hello world!";
    Integer[] begins = new Integer[] {2};
    Integer[] ends = new Integer[] {4};
    String[] replacements = new String[] {"babar"};
    String expected = "hebabaro world!";
    String result = DeidTools.replaceOffsets(monCR, begins, ends, replacements);
    System.out.println(result);
    assert (expected.equals(result));
  }

  @Test
  public void TestComplex() {
    // 0123456789
    String monCR = "hello world how are you!";
    Integer[] begins = new Integer[] {2, 23};
    Integer[] ends = new Integer[] {4, 24};
    String[] replacements = new String[] {"babar", " barbie"};
    String expected = "hebabaro world how are you barbie";
    String result = DeidTools.replaceOffsets(monCR, begins, ends, replacements);
    System.out.println(result);
    assert (expected.equals(result));
  }
  
 

  @Test
  public void TestFormat() {
    // 0123456789
    String entry = "2010-10-01";
    String result = DeidDateFormatter.format("MMMM", entry, "fr");
    String expected = "octobre";

    assert (expected.equals(result));
  }

  @Test
  public void TestExtractFormat() {
    // 0123456789
    String entry = "octobre";
    String result = DeidDateExtractor.extract(entry);
    String expected = "MMMM";
    System.out.println(result);
    assert (expected.equals(result));
  }
  
  @Test
  public void TestExtractFormat2() {
    // 0123456789
    String entry = "12/2012";
    String result = DeidDateExtractor.extract(entry);
    String expected = "MM/yyyy";
    System.out.println(result);
    assert (expected.equals(result));
  }
  
    @Test
    public void TestName() {
	// 0123456789
	String entry = "marc";
	String result = DeidNameFormatter.format(entry, "toto");
	System.out.println(result);
	assert ("Marc".equals(result));
    }
    
    @Test
    public void TestName1() {
	// 0123456789
	String entry = "MARC PAUL ";
	String result = DeidNameFormatter.format(entry, "Toto-Tata");
	System.out.println(result);
	assert ("Marc Paul".equals(result));
    }
    
    @Test 
    public void testSubstring() {
	String a = "abc";
	System.out.println(a.substring(2, 2) == null);
    }

}

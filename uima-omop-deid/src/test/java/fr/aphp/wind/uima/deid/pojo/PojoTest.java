/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.pojo;

import org.apache.uima.UIMAException;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class PojoTest {

    @Test
    public void testGetRow() throws IOException, UIMAException {
        DeidPojoProd pj = new DeidPojoProd();
        System.out.println(pj.getRow(1L,1L));
    }
}

/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.tools;

import java.util.ArrayList;

public class ResultTest {
	private int numberAnnotation = 0;
	private ArrayList<String> annot = new ArrayList<String>();

	public int getNumberAnnotation() {
		return numberAnnotation;
	}

	public void setNumberAnnotation(int numberAnnotation) {
		this.numberAnnotation = numberAnnotation;
	}



	public ArrayList<String> getAnnot() {
		return annot;
	}

	public void setAnnot(ArrayList<String> annot) {
		this.annot = annot;
	}

	public void addAnnot(String str) {
		this.numberAnnotation++;
		annot.add(str);
	}
	
public String toString(){
	return String.format("n=%s, %s", this.numberAnnotation, String.join("\n",this.annot));
	
}
	public boolean equals(Object o) {
		
		return 
				((ResultTest)o).getNumberAnnotation() == this.getNumberAnnotation()
				&& ((ResultTest)o).getAnnot().equals(this.getAnnot());
	}
	
}

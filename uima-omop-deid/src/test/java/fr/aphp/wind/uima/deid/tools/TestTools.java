/**
 * This file is part of UIMA-APHP.
 * UIMA-APHP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * UIMA-APHP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.tools;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.Normalizer;

import org.junit.Test;

import fr.aphp.wind.uima.core.stringutils.ReorganizeString;
import fr.aphp.wind.uima.core.stringutils.StringDistance;
import fr.aphp.wind.uima.core.stringutils.StringUtils;

/**
 * Unit test for simple App.
 */
public class TestTools {

	public static String stripAccents(String s) {
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}

	public static String normaliseCase(String s) {
		char firstLetter = s.charAt(0);
		String nextLetters = s.substring(1, s.length()).toLowerCase();
		String allLetters = firstLetter + nextLetters;
		return allLetters;
	}

	@Test public void testNormaliseCase() {
		assertTrue(normaliseCase("TOTO").equals("Toto"));
	}

	// a#zqé
	// z#aseé
	// e#zrd
	// r#eft
	// t#rgy
	// y#thuè
	// u#yjiè
	// i#ukoç
	// o#ilpçà
	// p#omà
	// q#asw
	// s#zqdwx
	// d#esfxc
	// f#rdgcv
	// g#tfhvb
	// h#ygjbn
	// j#uhkn
	// k#ijl
	// l#okm
	// m#plù
	// w#qsx
	// x#wsdc
	// c#xdfv
	// v#cfgb
	// b#vghn
	// n#bhj
	// é#az
	// è#yu
	// ç#io
	// à#op

	@Test public void testWeightedLevenshtein() {

		StringDistance distKernel = new StringDistance();
		assertTrue(distKernel.distance(normaliseCase("NOé"), normaliseCase("noa")) == 1.3);
		assertTrue(distKernel.distance(normaliseCase("agnis"), normaliseCase("aGnis")) == 0.0);
		assertTrue(distKernel.distance(normaliseCase("mangé"), normaliseCase("mange")) == 0.1);

		assertTrue(distKernel.isSimilar("Noé", "Noe", false));
		assertTrue(distKernel.isSimilar("Noé", "NOÉ", false));
		assertTrue(distKernel.isSimilar("Noé", "NOE", false));
		assertFalse(distKernel.isSimilar("Noé", "nOE", false));
		assertFalse(distKernel.isSimilar("Noé", "Noa", false));
		assertFalse(distKernel.isSimilar("Robert", "robart", false));
		assertTrue(distKernel.isSimilar("Robert", "Robart", false));
		assertFalse(distKernel.isSimilar("Robert", "Robvrt", false));
		assertTrue(distKernel.isSimilar("Nicolase", "Nocolese", false));
		assertTrue(distKernel.isSimilar("Nicolas", "Nocoles", false));
		assertTrue(distKernel.isSimilar("Delanoé", "Délanoe", false));
		assertFalse(distKernel.isSimilar("Arthur", "TurTur", false));
		assertTrue(distKernel.isSimilar("Arthur", "Arthru", false));
	}

	@Test public void testReorganize() {
		ReorganizeString reor;
		reor = new ReorganizeString("do", "do");
		reor.process();
		assertTrue(reor.getSubstitutionNumber().equals(0));
		assertTrue(reor.getReorganizedString().equals("do"));

		reor = new ReorganizeString("od", "do");
		reor.process();
		assertTrue(reor.getSubstitutionNumber().equals(1));
		assertTrue(reor.getReorganizedString().equals("do"));

		reor = new ReorganizeString("Nicolsa", "Nicolas");
		reor.process();
		assertTrue(reor.getSubstitutionNumber().equals(1));
		assertTrue(reor.getReorganizedString().equals("Nicolas"));

		reor = new ReorganizeString("dor", "dro");
		reor.process();
		assertTrue(reor.getSubstitutionNumber().equals(1));
		assertTrue(reor.getReorganizedString().equals("dro"));
	}

	@Test public void testFname() {
		assertTrue(StringUtils.lookFirstName("El"));
		assertTrue(StringUtils.lookLastName("GARCIA-LARNICOL"));
	}
}

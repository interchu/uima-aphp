/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.writer;

import java.util.HashMap;
import java.util.Map;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.CasUtil;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

/**
 * Converts a chunk annotations into IOB-style 
 * 
 *
 */
public class IobEncoderDeid
{  
    private Map<Integer, String> iobBeginMap;
    private Map<Integer, String> iobInsideMap;

    public IobEncoderDeid(CAS aCas, Type chunkType, Feature aChunkValueFeature)
    {
        super();
        
        // fill map for whole jcas in order to efficiently encode IOB
        iobBeginMap = new HashMap<Integer, String>();
        iobInsideMap = new HashMap<Integer, String>();

        for (AnnotationFS chunk : CasUtil.select(aCas, chunkType)) {
            String chunkValue = chunk.getStringValue(aChunkValueFeature);

            for (AnnotationFS token : CasUtil.selectCovered(aCas, CasUtil.getType(aCas, Token.class), chunk)) {

                if (token.getBegin() == chunk.getBegin()) {
                    iobBeginMap.put(token.getBegin(), chunkValue);
                }
                else {
                    iobInsideMap.put(token.getBegin(), chunkValue);
                }
            }
            for (AnnotationFS token : CasUtil.selectCovering(aCas, CasUtil.getType(aCas, Token.class), chunk)) {

                if (token.getBegin() == chunk.getBegin()) {
                    iobBeginMap.put(token.getBegin(), chunkValue);
                }
                else {
                    iobInsideMap.put(token.getBegin(), chunkValue);
                }
            }
        }
    }
    
    /**
     * Returns the IOB tag for a given token.
     * 
     * @param token
     *            a token.
     * @return the IOB tag.
     */
    public String encode(Token token)
    {
        if (iobBeginMap.containsKey(token.getBegin())) {
            return "B-" + iobBeginMap.get(token.getBegin());
        }
        
        if (iobInsideMap.containsKey(token.getBegin())) {
            return "I-" + iobInsideMap.get(token.getBegin());
        }
        
        return "O";
    }
}
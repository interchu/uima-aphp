/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.reasoner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import fr.aphp.wind.uima.core.casutils.CasTools;
import fr.aphp.wind.uima.core.stringutils.ListDeid;
import fr.aphp.wind.uima.core.stringutils.StringUtils;

public class GlobalAnnotationReasonerProd extends JCasAnnotator_ImplBase {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {

	Type tokenANnot = CasUtil.getType(aJCas.getCas(),
		"fr.aphp.wind.uima.type.SentenceFirstToken");

	/*
	 * GENERAL_UCUM A measure unit SHALL be preceded or follwed By a numeric
	 * value (3 after/before) Otherwise it is removed
	 */

	tokenANnot = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");
	ArrayList<Type> typesToFind = new ArrayList<Type>();
	typesToFind
		.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralNumericValue"));

	ArrayList<String> tokenToExclude = new ArrayList<String>();
	tokenToExclude.add("à");
	tokenToExclude.add("est");

	for (AnnotationFS ucumAnnot : CasUtil.select(aJCas.getCas(),
		CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralUcum"))) {// pour

	    boolean foundNumBefore = false;

	    foundNumBefore = CasTools.isNumberWordBetween(aJCas, ucumAnnot, typesToFind, tokenANnot,
		    -1, 3, tokenToExclude, false);
	    boolean foundNumAfter = CasTools.isNumberWordBetween(aJCas, ucumAnnot, typesToFind,
		    tokenANnot, 1, 3, tokenToExclude, false);

	    if (!foundNumBefore && !foundNumAfter) {
		((TOP) ucumAnnot).removeFromIndexes();
		logger.info(String.format("Remove generalUcum: >%s<", ucumAnnot.getCoveredText()));
	    } else {
		logger.info(String.format("Keep generalUcum: >%s<", ucumAnnot.getCoveredText()));
	    }
	}

	/*
	 * VILLES
	 * 
	 */

	// à|au|sur|vers (Ville)
	// OU
	// (Adresse) (CodePostal) (Ville)
	tokenANnot = CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Token");
	tokenToExclude = new ArrayList<String>();
	ArrayList<Type> typesNotToFind = new ArrayList<Type>();
	typesNotToFind.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralTitle"));
	typesNotToFind.add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralUcum"));

	for (AnnotationFS generalCity : CasUtil.select(aJCas.getCas(),
		CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralCity"))) {

	    boolean upperCase = StringUtils.isFirstUpperCase(generalCity.getCoveredText());

	    boolean foundPrefix = CasTools.isRegexAroundAnnot(aJCas, generalCity,
		    Arrays.asList(ListDeid.PREFIX_CITY_REGEX), -10);


	    boolean isGoodFollowedMedium = CasTools.isRegexAroundAnnot(aJCas, generalCity,
		    Arrays.asList(ListDeid.CITY_GOOD_SMEDIUM_FOL_REGEX), 10);

	    typesNotToFind.clear();
	    typesNotToFind
		    .add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralUcum"));
	    typesNotToFind
		    .add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralTitle"));
	    boolean foundUcumBefore = CasTools.isNumberWordBetween(aJCas, generalCity,
		    typesNotToFind, tokenANnot, -1, 1, tokenToExclude, false);
	    typesNotToFind.clear();
	    typesNotToFind
		    .add(CasUtil.getType(aJCas.getCas(), "fr.aphp.wind.uima.type.GeneralUcum"));
	    boolean foundUcumAfter = CasTools.isNumberWordBetween(aJCas, generalCity,
		    typesNotToFind, tokenANnot, 1, 2, tokenToExclude, false);
	    boolean ucumAround = (foundUcumBefore || foundUcumAfter);

	    if (ucumAround) {
		logger.info(String.format("Removed %s : %s (Uccum around)", generalCity.getType(),
			generalCity.getCoveredText()));
		((TOP) generalCity).removeFromIndexes();
	    }

	    if (generalCity.getCoveredText().contains(" ")
		    || generalCity.getCoveredText().contains("-") ) {
		logger.info(String.format("Keep %s : %s (multiword || zip around)",
			generalCity.getType(), generalCity.getCoveredText()));
		continue;
	    }

	    if (isGoodFollowedMedium) {
		logger.info(String.format("Keep %s : %s (Upper & prefix)", generalCity.getType(),
			generalCity.getCoveredText()));
		continue;
	    }

	    if (upperCase && foundPrefix) {
		logger.info(String.format("Keep %s : %s (Upper & prefix)", generalCity.getType(),
			generalCity.getCoveredText()));
		continue;
	    }
	    ((TOP) generalCity).removeFromIndexes();
	    logger.info(String.format("Removed %s : %s (other)", generalCity.getType(),
		    generalCity.getCoveredText()));

	}

    }

}

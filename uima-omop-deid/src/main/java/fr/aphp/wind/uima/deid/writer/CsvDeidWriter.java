/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.writer;

import static org.apache.commons.io.IOUtils.closeQuietly;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import de.tudarmstadt.ukp.dkpro.core.api.io.JCasFileWriter_ImplBase;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import fr.aphp.wind.uima.core.stringutils.StringUtils;
import fr.aphp.wind.uima.type.SourceDocumentInformation;

/**
 * <p>
 * Writes the CoNLL 2000 chunking format. The columns are separated by spaces.
 * </p>
 * 
 * <pre>
 * <code>
 * He        PRP  B-NP
 * reckons   VBZ  B-VP
 * the       DT   B-NP
 * current   JJ   I-NP
 * account   NN   I-NP
 * deficit   NN   I-NP
 * will      MD   B-VP
 * narrow    VB   I-VP
 * to        TO   B-PP
 * only      RB   B-NP
 * #         #    I-NP
 * 1.8       CD   I-NP
 * billion   CD   I-NP
 * in        IN   B-PP
 * September NNP  B-NP
 * .         .    O
 * </code>
 * </pre>
 * 
 * <ol>
 * <li>FORM - token</li>
 * <li>POSTAG - part-of-speech tag</li>
 * <li>CHUNK - chunk (BIO encoded)</li>
 * </ol>
 * 
 * <p>
 * Sentences are separated by a blank new line.
 * </p>
 * 
 * @see <a href="http://www.cnts.ua.ac.be/conll2000/chunking/">CoNLL 2000 shared task</a>
 */
@TypeCapability(inputs = {"de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData",
    "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence",
    "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
    "de.tudarmstadt.ukp.dkpro.core.api.syntax.type.chunk.Chunk"})
public class CsvDeidWriter extends JCasFileWriter_ImplBase {
  private Logger logger = Logger.getLogger(getClass().getName());
  /**
   * Name of configuration parameter that contains the character encoding used by the input files.
   */
  public static final String PARAM_ENCODING = ComponentParameters.PARAM_SOURCE_ENCODING;
  @ConfigurationParameter(name = PARAM_ENCODING, mandatory = true, defaultValue = "UTF-8")
  private String encoding;

  public static final String PARAM_FILENAME_SUFFIX = "filenameSuffix";
  @ConfigurationParameter(name = PARAM_FILENAME_SUFFIX, mandatory = true, defaultValue = ".conll")
  private String filenameSuffix;



  @Override
  public void process(JCas aJCas) throws AnalysisEngineProcessException {
    PrintWriter out = null;
    try {
	//System.out.println(1);
      SourceDocumentInformation srcDocInfo = new SourceDocumentInformation(aJCas);
      logger.warn(getUriShort(aJCas));
      out = new PrintWriter(new OutputStreamWriter(getOutputStream(aJCas, ".csv"), encoding));
      convert(aJCas, out);
    } catch (Exception e) {

      throw new AnalysisEngineProcessException(e);
    } finally {
      closeQuietly(out);
    }
  }

  private void convert(JCas aJCas, PrintWriter aOut) {



    // , "fr.aphp.wind.uima.type.GeneralFirstName -> prenom"
    // , "fr.aphp.wind.uima.type.GeneralLastName -> nom"
    // , "fr.aphp.wind.uima.type.GeneralDate -> date"
    // , "fr.aphp.wind.uima.type.GeneralEMail -> mail"
    // , "fr.aphp.wind.uima.type.GeneralNbIpp -> ipp"
    // , "fr.aphp.wind.uima.type.GeneralNbNda -> nda"
    // , "fr.aphp.wind.uima.type.GeneralZipCode -> zip"
    // , "fr.aphp.wind.uima.type.GeneralCity -> ville"
    // , "fr.aphp.wind.uima.type.GeneralStreet -> adresse"
    // , "fr.aphp.wind.uima.type.GeneralPhoneNumber -> tel"
    // , "fr.aphp.wind.uima.type.GeneralNbSecu -> secu"

    //
    // FOOTER
    // Sans texte pour ne pas polluer
    for (fr.aphp.wind.uima.type.NoteHeader ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.NoteHeader.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "header", ner.getBegin(),
          ner.getEnd(), ""));
    }
    //
    // HEADER
    // Sa,s texte pour ne pas polluer
    for (fr.aphp.wind.uima.type.NoteFooter ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.NoteFooter.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "footer", ner.getBegin(),
          ner.getEnd(), ""));
    }

    //
    // FIRSTNAME
    //
    for (fr.aphp.wind.uima.type.GeneralFirstName ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralFirstName.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "prenom", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // LASTNAME
    //
    for (fr.aphp.wind.uima.type.GeneralLastName ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralLastName.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "nom", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // MAIL
    //
    for (fr.aphp.wind.uima.type.GeneralEMail ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralEMail.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "mail", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // IPP
    //
    for (fr.aphp.wind.uima.type.GeneralNbIpp ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralNbIpp.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "ipp", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // NDA
    //
    for (fr.aphp.wind.uima.type.GeneralNbNda ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralNbNda.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "nda", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // SECU
    //
    for (fr.aphp.wind.uima.type.GeneralNbSecu ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralNbSecu.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "secu", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // ZIP
    //
    for (fr.aphp.wind.uima.type.GeneralZipCode ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralZipCode.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "zip", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // STREET
    //
    for (fr.aphp.wind.uima.type.GeneralStreet ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralStreet.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "adresse", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // TEL
    //
    for (fr.aphp.wind.uima.type.GeneralPhoneNumber ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralPhoneNumber.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "tel", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // VILLE
    //
    for (fr.aphp.wind.uima.type.GeneralCity ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralCity.class)) {
      aOut.println(String.format("%s;%s;%s;%s;%s;", getUriShort(aJCas), "ville", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText())));
    }

    //
    // DATE
    //
    for (fr.aphp.wind.uima.type.GeneralDate ner : JCasUtil.select(aJCas,
        fr.aphp.wind.uima.type.GeneralDate.class)) {
      Type annotationType =
          CasUtil.getType(aJCas.getCas(), "de.unihd.dbs.uima.types.heideltime.Timex3");
      String timexValue = null;
      for (AnnotationFS sc : CasUtil.selectCovering(aJCas.getCas(), annotationType, ner)) {
        timexValue = ((de.unihd.dbs.uima.types.heideltime.Timex3) sc).getTimexValue();
      }
      aOut.println(String.format("%s;%s;%s;%s;%s;%s", getUriShort(aJCas), "date", ner.getBegin(),
          ner.getEnd(), escape(ner.getCoveredText()), escape(timexValue)));
    }



  }

  private String escape(String value) {
    if(value == null) {
      return null;
    }
    return "\"" + value.replaceAll("\"", "\"\"") + "\"";
  }

  private String getUri(JCas aJCas) {
    FSIterator<Annotation> a = aJCas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
    SourceDocumentInformation b = null;
    if (a.hasNext()) {
      b = (SourceDocumentInformation) a.next();
      return b.getUri();
    }
    // file:/tmp/input/CR-CONS_24081078.txt
    return DocumentMetaData.get(aJCas).getDocumentUri().replaceAll(".*?([^/]+).txt$", "$1");
  }

  private String getUriShort(JCas aJCas) {
    // CR-CONS_24081078
    // CR-CONS
    return getUri(aJCas).replaceAll("^.*_(\\d+)$", "$1");
  }

}

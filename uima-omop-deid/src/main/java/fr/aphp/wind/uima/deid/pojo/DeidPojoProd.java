/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.pojo;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.factory.JCasFactory.createJCas;

import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.opencsv.CSVReader;

import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.io.brat.BratWriter;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import fr.aphp.wind.uima.deid.annotator.DatesMatcherAE;
import fr.aphp.wind.uima.deid.annotator.DemographicsAnnotationProdAE;
import fr.aphp.wind.uima.deid.annotator.HeadersAnnotationAE;
import fr.aphp.wind.uima.deid.annotator.RegexCaptureAE;
import fr.aphp.wind.uima.deid.writer.BratDeidWriter;
import fr.aphp.wind.uima.mapper.MapperAnnotator;
import fr.aphp.wind.uima.mapper.UimaMapper;
import fr.aphp.wind.uima.type.NoteDeidDemographics;
import scala.Tuple9;

public class DeidPojoProd implements Serializable {

	private static final long serialVersionUID = 1L;
	private transient AnalysisEngineDescription stanfordSegmenter;
	private transient AnalysisEngineDescription bratWriter;
	private transient AnalysisEngineDescription demographicsAnnotation;
	private transient AnalysisEngineDescription posTaggerNlp;
	private transient JCas aJCas;
	private String outPath;
	private JsonNode json;
	private AnalysisEngineDescription heidelTime;
	private AnalysisEngineDescription mapperAnnotator;
	private NoteDeidDemographics demographicsDocumentAnnotation;
	private AnalysisEngineDescription mapperNoteDeid;
	private String inPath;
	private CollectionReaderDescription readerTxt;
	private AnalysisEngineDescription datesMatcher;
	private AnalysisEngineDescription mapperGeneral;
	private AnalysisEngineDescription headersAnnotation;
	private AnalysisEngineDescription demographicsAnnotationProd;
	private AnalysisEngineDescription regexCaptureAe;
	private AnalysisEngineDescription dictTagger;

	// For UIMAFIT
	public DeidPojoProd(String in, String out) throws IOException, UIMAException {
		this.outPath = out;
		this.inPath = in;
		this.initialize();
		this.initializeReadFile();
		this.initializeWriter();
		this.initializeDict();
		this.initializeRegexCapture();
	}

	private void initializeDict() throws IOException, InvalidXMLException {
		// DICTIONnARY
			dictTagger = AnalysisEngineFactory.createEngineDescriptionFromPath("DictionaryAnnotator.xml");
	}

	// For SPARK
	public DeidPojoProd() throws IOException, UIMAException {
		this.initialize();
		this.initializeRegexCapture();
	}


	public void initialize() throws IOException, UIMAException {

		aJCas = createJCas();

	// opennlp pos tagger
	//	posTaggerNlp = createEngineDescription(OpenNlpPosTagger.class,
	//			OpenNlpPosTagger.PARAM_LANGUAGE, "en", OpenNlpPosTagger.PARAM_MODEL_LOCATION,
	//			"fr-pos.bin");

		// the stanford segmenter
		stanfordSegmenter = createEngineDescription(StanfordSegmenter.class,
				StanfordSegmenter.PARAM_LANGUAGE, "fr"
				// , StanfordSegmenter.PARAM_BOUNDARIES_TO_DISCARD, tokenDiscard
				, StanfordSegmenter.PARAM_NEWLINE_IS_SENTENCE_BREAK, "TWO_CONSECUTIVE");

		demographicsAnnotationProd = createEngineDescription(DemographicsAnnotationProdAE.class);

		// look for header / footer
		headersAnnotation = createEngineDescription(HeadersAnnotationAE.class);

		mapperNoteDeid = createEngineDescription(UimaMapper.class,
				UimaMapper.PARAM_CONFIG_FILE_PATH, "mapperNoteDeid.json");

		mapperGeneral = createEngineDescription(UimaMapper.class, UimaMapper.PARAM_CONFIG_FILE_PATH,
				"mapperGeneral.json");

		// mapps sentenses & tokens
		mapperAnnotator = createEngineDescription(MapperAnnotator.class);

		heidelTime = AnalysisEngineFactory.createEngineDescriptionFromPath("HeidelTime.xml");

		// DATES
		datesMatcher = createEngineDescription(DatesMatcherAE.class);

	}

	public void initializeRegexCapture() throws ResourceInitializationException, IOException {

		CSVReader reader = new CSVReader(new FileReader("regexCaptureProd.csv"), ",".charAt(0),
				"\"".charAt(0), true);
		String[] nextLine;
		ArrayList<String> arrayListAnnoName = new ArrayList<String>();
		ArrayList<String> arrayListRegexCapture = new ArrayList<String>();

		while ((nextLine = reader.readNext()) != null) {
			arrayListAnnoName.add(nextLine[0]);
			arrayListRegexCapture.add(nextLine[1]);
		}

		String[] annoName = arrayListAnnoName.toArray(new String[arrayListAnnoName.size()]);
		String[] regexCapture = arrayListRegexCapture
				.toArray(new String[arrayListRegexCapture.size()]);
		regexCaptureAe = createEngineDescription(RegexCaptureAE.class,
				RegexCaptureAE.PARAM_ARRAY_ANNO_NAME, annoName, RegexCaptureAE.PARAM_ARRAY_PATTERN,
				regexCapture);

	}

	public void initializeWriter() throws ResourceInitializationException {

		Set<String> set = new HashSet<String>();

		set.add("fr.aphp.wind.uima.type.GeneralPhoneNumber");
		set.add("fr.aphp.wind.uima.type.GeneralNbNda");
		set.add("fr.aphp.wind.uima.type.GeneralNbIpp");
		set.add("fr.aphp.wind.uima.type.GeneralNbSecu");
		set.add("fr.aphp.wind.uima.type.GeneralStreet");
		set.add("fr.aphp.wind.uima.type.NoteHeader");
		set.add("fr.aphp.wind.uima.type.NoteFooter");
		set.add("fr.aphp.wind.uima.type.GeneralDate");
		set.add("fr.aphp.wind.uima.type.GeneralFirstName");
		set.add("fr.aphp.wind.uima.type.GeneralLastName");
		set.add("fr.aphp.wind.uima.type.GeneralZipCode");
		set.add("fr.aphp.wind.uima.type.GeneralCity");
		set.add("fr.aphp.wind.uima.type.GeneralEMail");

		bratWriter = createEngineDescription(BratDeidWriter.class,
				BratDeidWriter.PARAM_TARGET_LOCATION, this.outPath,
				BratDeidWriter.PARAM_TEXT_ANNOTATION_TYPES, set, BratDeidWriter.PARAM_ALL_TYPES,
				false, BratDeidWriter.PARAM_ENABLE_TYPE_MAPPINGS, true,
				BratDeidWriter.PARAM_TYPE_MAPPINGS,
				new String[] { "fr.aphp.wind.uima.type.NoteHeader -> HEADER",
						"fr.aphp.wind.uima.type.NoteFooter -> FOOTER",
						"fr.aphp.wind.uima.type.GeneralFirstName -> PRENOM",
						"fr.aphp.wind.uima.type.GeneralLastName -> NOM",
						"fr.aphp.wind.uima.type.GeneralDate -> DATE",
						"fr.aphp.wind.uima.type.GeneralEMail -> MAIL",
						"fr.aphp.wind.uima.type.GeneralNbIpp -> IPP",
						"fr.aphp.wind.uima.type.GeneralNbNda -> NDA",
						"fr.aphp.wind.uima.type.GeneralZipCode -> ZIP",
						"fr.aphp.wind.uima.type.GeneralCity -> VILLE",
						"fr.aphp.wind.uima.type.GeneralStreet -> ADRESSE",
						"fr.aphp.wind.uima.type.GeneralPhoneNumber -> TEL",
						"fr.aphp.wind.uima.type.GeneralNbSecu -> SECU" },
				BratWriter.PARAM_OVERWRITE, true);
	}

	public void initializeReadFile() throws ResourceInitializationException {
		readerTxt = createReaderDescription(TextReader.class, TextReader.PARAM_SOURCE_LOCATION,
				this.inPath, TextReader.PARAM_PATTERNS, "*", TextReader.PARAM_LANGUAGE, "fr");
	}

	public void runProductionPipelineWithReader()
			throws UIMAException, IOException, SAXException, CpeDescriptorException {

		SimplePipeline.runPipeline(
				readerTxt
				, stanfordSegmenter
//				, posTaggerNlp
				, mapperAnnotator
				, heidelTime
				, datesMatcher
				, regexCaptureAe
				, demographicsAnnotationProd
				, headersAnnotation
				, dictTagger
				, mapperNoteDeid
				, mapperGeneral
				, bratWriter
		);
	}

	public void runProductionPipelineWithoutReader()
			throws UIMAException, IOException, SAXException, CpeDescriptorException {
		SimplePipeline.runPipeline(
				aJCas
				, stanfordSegmenter
			//  , posTaggerNlp
				, mapperAnnotator
				, heidelTime
				, datesMatcher
				, regexCaptureAe
				, demographicsAnnotationProd
				, headersAnnotation
			//  , dictTagger
				, mapperNoteDeid
				, mapperGeneral
		);
	}

	public List<Tuple9<Long, Long, Integer, Integer, String, String, String, String, String>>  analyzeText(
			Long ids_fait_doc
			, Long ids_eds_patient
			, String text
			, String demoJson)
			throws Exception {
		long startTime = System.nanoTime();
		// Get the json
		String location = UUID.randomUUID().toString().replaceAll("-", "");

		aJCas.reset();
		initCasDemographics(demoJson);
		initCasForDkpro(location, demographicsDocumentAnnotation.getFileName());
		initCasDemographics(demoJson);// après dkpro car sinon reset

		aJCas.setDocumentText(text);
		// run the pipeline
		runProductionPipelineWithoutReader();

		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		System.out.println(String.format("%s in %s", ids_fait_doc, duration/1000000));

		//return extractCsv(ids_fait_doc, ids_eds_patient);
		return getRow(ids_fait_doc, ids_eds_patient);
	}

	public  String  analyzeTextCsv(
			Long ids_fait_doc
			, Long ids_eds_patient
			, String text
			, String demoJson)
			throws Exception {
		// Get the json
		String location = UUID.randomUUID().toString().replaceAll("-", "");

		aJCas.reset();
		initCasDemographics(demoJson);
		initCasForDkpro(location, demographicsDocumentAnnotation.getFileName());
		initCasDemographics(demoJson);// après dkpro car sinon reset

		aJCas.setDocumentText(text);
		// run the pipeline
		runProductionPipelineWithoutReader();

		return  extractCsv(ids_fait_doc, ids_eds_patient);
	}

	protected void initCasForDkpro(String path, String fileName) {
		// General
		aJCas.setDocumentLanguage("fr");

		// Set the document metadata for dkpro
		DocumentMetaData docMetaData = DocumentMetaData.create(aJCas);
		docMetaData.setDocumentTitle("documentest");
		docMetaData.setDocumentUri("/tmp#");
		docMetaData.setDocumentId(fileName);
	}

	protected void initCasDemographics(String demoJson)
			throws JsonProcessingException, IOException {
		// Add personnal informations
		demographicsDocumentAnnotation = new NoteDeidDemographics(aJCas);
		demographicsDocumentAnnotation.setFileName("N/A");
		demographicsDocumentAnnotation.setJson(demoJson);
		demographicsDocumentAnnotation.addToIndexes();
	}

	private Tuple9<Long, Long, Integer, Integer, String, String, String, String, String> createRow(Long note_id, Long person_id, String note_nlp_source_value,
																								   int offset_begin, int offset_end, String snipped, String date_infered){
		return new Tuple9<Long, Long, Integer, Integer, String, String, String, String, String>(note_id, person_id,  offset_begin,
				offset_end, note_nlp_source_value, snipped, null, date_infered, "uima deid");
	}

	public ArrayList<Tuple9<Long, Long, Integer, Integer, String, String, String, String, String>> getRow(Long ids_doc, Long ids_eds_patient){

		ArrayList<Tuple9<Long, Long, Integer, Integer, String, String, String, String, String>> list = new ArrayList<Tuple9<Long, Long, Integer, Integer, String, String, String, String, String>>();

		//
		// FOOTER
		// Sans texte pour ne pas polluer
		for (fr.aphp.wind.uima.type.NoteHeader ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.NoteHeader.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "HEADER", ner.getBegin(), ner.getEnd(),
					null, null));
		}
		//
		// HEADER
		// Sa,s texte pour ne pas polluer
		for (fr.aphp.wind.uima.type.NoteFooter ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.NoteFooter.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "FOOTER", ner.getBegin(), ner.getEnd(),
					null, null));
		}

		//
		// FIRSTNAME
		//
		for (fr.aphp.wind.uima.type.GeneralFirstName ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralFirstName.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "PRENOM", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// LASTNAME
		//
		for (fr.aphp.wind.uima.type.GeneralLastName ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralLastName.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "NOM", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// MAIL
		//
		for (fr.aphp.wind.uima.type.GeneralEMail ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralEMail.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "MAIL", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// IPP
		//
		for (fr.aphp.wind.uima.type.GeneralNbIpp ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbIpp.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "IPP", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// NDA
		//
		for (fr.aphp.wind.uima.type.GeneralNbNda ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbNda.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "NDA", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// SECU
		//
		for (fr.aphp.wind.uima.type.GeneralNbSecu ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralNbSecu.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "SECU", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// ZIP
		//
		for (fr.aphp.wind.uima.type.GeneralZipCode ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralZipCode.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "ZIP", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// STREET
		//
		for (fr.aphp.wind.uima.type.GeneralStreet ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralStreet.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "ADRESSE", ner.getBegin(),
					ner.getEnd(), ner.getCoveredText(), null));
		}

		//
		// TEL
		//
		for (fr.aphp.wind.uima.type.GeneralPhoneNumber ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralPhoneNumber.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "TEL", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// VILLE
		//
		for (fr.aphp.wind.uima.type.GeneralCity ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralCity.class)) {
			list.add(createRow(ids_doc, ids_eds_patient, "VILLE", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), null));
		}

		//
		// DATE
		//
		for (fr.aphp.wind.uima.type.GeneralDate ner : JCasUtil.select(aJCas,
				fr.aphp.wind.uima.type.GeneralDate.class)) {
			Type annotationType = CasUtil.getType(aJCas.getCas(),
					"de.unihd.dbs.uima.types.heideltime.Timex3");
			String timexValue = null;
			for (AnnotationFS sc : CasUtil.selectCovering(aJCas.getCas(), annotationType, ner)) {
				timexValue = ((de.unihd.dbs.uima.types.heideltime.Timex3) sc).getTimexValue();
			}
			list.add(createRow(ids_doc, ids_eds_patient, "DATE", ner.getBegin(), ner.getEnd(),
					ner.getCoveredText(), timexValue));
		}

		return list;

	}

	private String csvFormatter(Long note_id, Long person_id, String note_nlp_source_value,
								int offset_begin, int offset_end, String snipped, String date_infered) {
		String pat = "%s;%s;%s;%s;%s;%s;%s;%s;%s\n";
		return String.format(pat, note_id, person_id,  offset_begin,
				offset_end, note_nlp_source_value, escape(snipped), "",escape(date_infered), "uima deid");
	}

	protected String extractCsv(Long ids_doc, Long ids_eds_patient) throws Exception {
		StringBuilder sb = new StringBuilder();
		try {
			//
			// FOOTER
			// Sans texte pour ne pas polluer
			for (fr.aphp.wind.uima.type.NoteHeader ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.NoteHeader.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "HEADER", ner.getBegin(), ner.getEnd(),
						"", ""));
			}
			//
			// HEADER
			// Sa,s texte pour ne pas polluer
			for (fr.aphp.wind.uima.type.NoteFooter ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.NoteFooter.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "FOOTER", ner.getBegin(), ner.getEnd(),
						"", ""));
			}

			//
			// FIRSTNAME
			//
			for (fr.aphp.wind.uima.type.GeneralFirstName ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralFirstName.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "PRENOM", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// LASTNAME
			//
			for (fr.aphp.wind.uima.type.GeneralLastName ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralLastName.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "NOM", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// MAIL
			//
			for (fr.aphp.wind.uima.type.GeneralEMail ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralEMail.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "MAIL", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// IPP
			//
			for (fr.aphp.wind.uima.type.GeneralNbIpp ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralNbIpp.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "IPP", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// NDA
			//
			for (fr.aphp.wind.uima.type.GeneralNbNda ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralNbNda.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "NDA", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// SECU
			//
			for (fr.aphp.wind.uima.type.GeneralNbSecu ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralNbSecu.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "SECU", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// ZIP
			//
			for (fr.aphp.wind.uima.type.GeneralZipCode ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralZipCode.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "ZIP", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// STREET
			//
			for (fr.aphp.wind.uima.type.GeneralStreet ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralStreet.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "ADRESSE", ner.getBegin(),
						ner.getEnd(), ner.getCoveredText(), ""));
			}

			//
			// TEL
			//
			for (fr.aphp.wind.uima.type.GeneralPhoneNumber ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralPhoneNumber.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "TEL", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// VILLE
			//
			for (fr.aphp.wind.uima.type.GeneralCity ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralCity.class)) {
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "VILLE", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), ""));
			}

			//
			// DATE
			//
			for (fr.aphp.wind.uima.type.GeneralDate ner : JCasUtil.select(aJCas,
					fr.aphp.wind.uima.type.GeneralDate.class)) {
				Type annotationType = CasUtil.getType(aJCas.getCas(),
						"de.unihd.dbs.uima.types.heideltime.Timex3");
				String timexValue = null;
				for (AnnotationFS sc : CasUtil.selectCovering(aJCas.getCas(), annotationType, ner)) {
					timexValue = ((de.unihd.dbs.uima.types.heideltime.Timex3) sc).getTimexValue();
				}
				sb.append(csvFormatter(ids_doc, ids_eds_patient, "DATE", ner.getBegin(), ner.getEnd(),
						ner.getCoveredText(), timexValue));
			}
		}catch(Exception e) {
			throw new Exception(
					String.format("%s\n taille:%s\nexeption:%s", aJCas.getDocumentText(), aJCas.getDocumentText().length(),e.getMessage())
			) ;
		}

		return sb.toString().replaceFirst("\n$", "");
	}

	private String escape(String value) {
		if (value == null) {
			return null;
		}
		return "\"" + value.replaceAll("\"", "\"\"") + "\"";
	}

}

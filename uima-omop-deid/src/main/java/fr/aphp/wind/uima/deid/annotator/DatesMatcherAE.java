/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.deid.annotator;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import fr.aphp.wind.uima.deid.tools.TimexTools;

public class DatesMatcherAE extends JCasAnnotator_ImplBase {
	/**
	 * Specify the suffix of text output files. Default value <code>.txt</code>.
	 * If the suffix is not needed, provide an empty string as value.
	 */
	private Logger logger = Logger.getLogger(getClass().getName());


	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		for (de.unihd.dbs.uima.types.heideltime.Timex3 timex : JCasUtil.select(aJCas,
				de.unihd.dbs.uima.types.heideltime.Timex3.class)) {
			if(TimexTools.isDeidTimex(timex)){// only track dates
				String annot = "fr.aphp.wind.uima.type.GeneralDate";

				Type tokenType = CasUtil.getAnnotationType(aJCas.getCas(), annot);
				FeatureStructure fs = aJCas.getCas().createAnnotation(tokenType, timex.getBegin(),
						timex.getEnd());
				aJCas.getIndexRepository().addFS(fs);
				logger.debug(String.format("Add Timex: >%s< (%s)", timex.getCoveredText(),timex.getFoundByRule()));
			}
		}
	}
}

/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.aphp.wind.uima.mapper;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.cas.TOP;

public class ActionOverwrite implements Action {

	private String superType;
	private String[] supraTypes;

	public ActionOverwrite(String superType, String[] supraTypes) {

		this.superType = superType;
		this.supraTypes = supraTypes;
	}

	public ActionOverwrite() {
	}

	public String getSuperType() {
		return superType;
	}

	public void setSuperType(String superType) {
		this.superType = superType;
	}

	public String[] getSupraTypes() {
		return supraTypes;
	}

	public void setSupraTypes(String[] supraTypes) {
		this.supraTypes = supraTypes;
	}

	@Override
	public void process(CAS aCas) {
		// TODO Auto-generated method stub
		Type annotationType = CasUtil.getType(aCas, superType);
		Type supraTypeAnn;
		for (AnnotationFS sp : CasUtil.select(aCas, annotationType)) {
			for (String supraType : supraTypes) {
				if(!supraType.equals(superType)){
				supraTypeAnn = CasUtil.getType(aCas, supraType);
				for (AnnotationFS sc : CasUtil.selectCovered(aCas, supraTypeAnn, sp)) {
					TOP test = (TOP) sc;
					((TOP) sc).removeFromIndexes();
				}
				for (AnnotationFS sc : CasUtil.selectCovering(aCas, supraTypeAnn, sp)) {
					TOP test = (TOP) sc;
					((TOP) sc).removeFromIndexes();
				}
				}
			}
		}
	}
}

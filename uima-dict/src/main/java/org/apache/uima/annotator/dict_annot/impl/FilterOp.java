/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.impl;

/**
 * Filter operators
 */
public enum FilterOp {

   NULL, NOT_NULL, EQUALS, NOT_EQUALS, LESS, LESS_EQ, GREATER, GREATER_EQ;

   public String toString() {
      switch (this) {
      case NULL: {
         return "null";
      }
      case NOT_NULL: {
         return "!null";
      }
      case EQUALS: {
         return "=";
      }
      case NOT_EQUALS: {
         return "!=";
      }
      case LESS: {
         return "<";
      }
      case LESS_EQ: {
         return "<=";
      }
      case GREATER: {
         return "<";
      }
      case GREATER_EQ: {
         return "<=";
      }
      default: {
         return "";
      }
      }
   }
}

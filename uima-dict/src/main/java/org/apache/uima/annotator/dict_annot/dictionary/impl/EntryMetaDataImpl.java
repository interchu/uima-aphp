/**
 *     This file is part of UIMA-APHP.
 *
 *     UIMA-APHP is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     UIMA-APHP is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with UIMA-APHP.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.apache.uima.annotator.dict_annot.dictionary.impl;

import org.apache.uima.annotator.dict_annot.dictionary.EntryMetaData;

/**
 * Implementation of the EntryMetaData interface.
 */
public class EntryMetaDataImpl implements EntryMetaData {

   // unique ID of the current dictionary entry
   private int id;

   /**
    * creates a new EntryMetaDataImpl object with a unique ID.
    * 
    * @param id
    *           unique ID
    */
   public EntryMetaDataImpl(int id) {
      this.id = id;
   }

   /*
    * (non-Javadoc)
    * 
    * @see org.apache.uima.annotator.dict_annot.EntryMetaData#getId()
    */
   public int getId() {
      return this.id;
   }
}
